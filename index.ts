import * as functions from 'firebase-functions';
 import * as admin from 'firebase-admin';

 //import { UserDimensions } from 'firebase-functions/lib/providers/analytics';
 //import { DataSnapshot } from 'firebase-functions/lib/providers/database';

//constants that describe the state of a friend invite
const sInviteSent = "invite sent";
const sInviteReceived = "invited";
const sInviteRejected = "invite rejected";
const sRejectedInvite = "rejected invite";
const sFriend = "friend";
const sUnfriendSent = "unfriended";
const sUnfriendReceived = "unfriend received";
const sInviteCancel = "cancelled invite";


//FCM action constants for Android
const CHANGE_FRIEND_STATUS = "com.crewconnect.CHANGE_FRIEND_STATUS";
const NEW_FRIEND_CHAT_MESSAGE = "com.crewconnect.NEW_FRIEND_CHAT_MESSAGE";
const NEW_GROUP_CHAT_MESSAGE = "com.crewconnect.NEW_GROUP_CHAT_MESSAGE";

//constants for what type of notification should(or shouldn't) be sent
const sFriendChatNotify = "friendChatNotify";
const sFlightChatNotify = "flightChatNotify";
const sLayoverChatNotify = "layoverChatNotify";
const sFriendStatusNotify = "friendStatusNotify";

//constants for type of group chat
const sFlightType = "flight";
const sLayoverType = "layover";

//possible friend actions:
//  invite friend, cancel invite, accept invite, reject invite, unfriend, 

//console.log('authorization: ', context.auth)
console.log('hy');
/////////////////////////////
// admin.initializeApp({
//     credential: admin.credential.applicationDefault(),
//     databaseURL: 'https://crewconnect-na.firebaseio.com'
//   });
////////////////////////////

//--------------------------------- friendStatus Functions --------------------------------------------------------
//--------------------------------- friendStatus Functions --------------------------------------------------------
//--------------------------------- friendStatus Functions --------------------------------------------------------

export const sendFriendInvite = functions.https.onCall(async (data, context) => {

    //check preconditions: Authorization
    checkAuth(context)

    const dbRoot = data.dbRoot
    let myUid = ""
    
    if(context.auth === undefined) {
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
        
    } else {
        myUid = context.auth.uid;
    }
    const fuid = data.friendID

    //read current friend status to ensure the invite is allowed:
    //   NOT: unfriend received or invite rejected or invite sent
    const currentStatus = await getCuurentStatus(myUid, fuid, dbRoot)

    switch (currentStatus) {
        case sInviteReceived:
            //accept invite
            console.log("accept invite")
            await acceptInvite(myUid, fuid, dbRoot)
            break

        case sInviteSent:
            console.log("You've already invited that person")
            throw new functions.https.HttpsError('failed-precondition', 'You\'ve already invited that person');

        case sInviteRejected:
            console.log("That person has rejected an invite from you")
            throw new functions.https.HttpsError('failed-precondition', 'That person has rejected an invite from you');

        case sFriend:
            console.log("You are already friends with that person")
            throw new functions.https.HttpsError('failed-precondition', 'You are already friends with that person');

        case sUnfriendReceived:
            console.log("That person has unfriended you and you cannot send an invite")
            throw new functions.https.HttpsError('failed-precondition', 'That person has unfriended you and you cannot send an invite');

        case sInviteCancel:
        case sUnfriendSent:
        case sRejectedInvite:
        case "":
            console.log("send invite")
            return sendInvite(myUid, fuid, dbRoot)
    }
    return { text: 'return value' }
});

export const cancelFriendInvite = functions.https.onCall(async (data, context) => {

    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    let myUid = ""
    
    if(context.auth === undefined) {
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
        
    } else {
        myUid = context.auth.uid;
    }
    const fuid = data.friendID

    //get current status
    const currentStatus = await getCuurentStatus(myUid, fuid, baseDBPath)

    if (currentStatus === sInviteSent) {
        //cancel invite
        //get the users name
        const userName = await getUserName(myUid, baseDBPath)
        console.log('userName: ' + userName)

        //send FCM
        const message =
        {
            token: "",
            notification: {
                title: "Friend Invite Cancelled",
                body: userName + ' has cancelled their invitation to be friends'
            },
            "apns": {
                "headers":{
    
                    "apns-priority": "10",
                },
                "payload":{
                    "aps":{
                        "sound": "default",
                        "badge": 1
                    }
                }
            },
            data: {
                action: CHANGE_FRIEND_STATUS
            }
        }

        await sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)
        await updateFriendStatus(myUid, fuid, sInviteCancel, sInviteCancel, baseDBPath);
    }
    else {
        console.log("You have not invited that person")
        throw new functions.https.HttpsError('failed-precondition', 'You have not invited that person');
    }
    return { text: 'return value' }
});

export const removeUnfriended = functions.https.onCall(async (data, context) => {

    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    let myUid = ""
    
    if(context.auth === undefined) {
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
        
    } else {
        myUid = context.auth.uid;
    }
    const fuid = data.friendID

    //get current status
    const currentStatus = await getCuurentStatus(myUid, fuid, baseDBPath)

    if (currentStatus === sUnfriendSent || currentStatus === sUnfriendReceived) {
        //unfriendSent
        const userPath = baseDBPath + '/Users/' + myUid + '/FriendsList/' + fuid
        console.log("userPath: " + userPath)
    
        const friendPath = baseDBPath + '/Users/' + fuid + '/FriendsList/' + myUid
        console.log("friendPath: " + friendPath)
    
        //write to the Database
        const db = admin.database();
        const userRef = db.ref(userPath);
        const friendRef = db.ref(friendPath)
        await userRef.remove()
        return friendRef.remove()

    }
    else {
        console.log("You have not unfriended that person")
        throw new functions.https.HttpsError('failed-precondition', 'You have not unfriended that person');
    }
    // return { text: 'return value' }
});

export const unFriend = functions.https.onCall(async (data, context) => {

    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    let myUid = ""
    
    if(context.auth === undefined) {
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
        
    } else {
        myUid = context.auth.uid;
    }
    const fuid = data.friendID

    //get current status
    const currentStatus = await getCuurentStatus(myUid, fuid, baseDBPath)

    if (currentStatus === sFriend) {
        //unfriend
        //get the users name
        const userName = await getUserName(myUid, baseDBPath)
        console.log('userName: ' + userName)

        //send FCM
        const message =
        {
            token: "",
            notification: {
                title: "Unfriended",
                body: userName + ' is no longer friends with you'
            },
            "apns": {
                "headers":{
    
                    "apns-priority": "10",
                },
                "payload":{
                    "aps":{
                        "sound": "default",
                        "badge": 1
                    }
                }
            },
            data: {
                action: CHANGE_FRIEND_STATUS
            }
        }

        await sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)
        await updateFriendStatus(myUid, fuid, sUnfriendSent, sUnfriendReceived, baseDBPath);
    }
    else {
        console.log("Your are not friends with that person")
        throw new functions.https.HttpsError('failed-precondition', 'Your are not friends with that person');
    }
    return { text: 'return value' }
});

export const rejectFriendInvite = functions.https.onCall(async (data, context) => {

    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    let myUid = ""
    
    if(context.auth === undefined) {
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
        
    } else {
        myUid = context.auth.uid;
    }
    const fuid = data.friendID

    //reject friend request
    const currentStatus = await getCuurentStatus(myUid, fuid, baseDBPath)

    if (currentStatus === sInviteReceived) {
        //reject invite
        //get the users name
        const userName = await getUserName(myUid, baseDBPath)
        console.log('userName: ' + userName)

        //send FCM
        const message =
        {
            token: "",
            notification: {
                title: "Declined Friend Invite",
                body: userName + ' has declined your friend request'
            },
            "apns": {
                "headers":{
    
                    "apns-priority": "10",
                },
                "payload":{
                    "aps":{
                        "sound": "default",
                        "badge": 1
                    }
                }
            },
            data: {
                action: CHANGE_FRIEND_STATUS
            }
        }

        await sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)
        await updateFriendStatus(myUid, fuid, sRejectedInvite, sInviteRejected, baseDBPath);
    }
    else {
        console.log("That person has not invited you")
        throw new functions.https.HttpsError('failed-precondition', 'That person has not invited you');
    }
    return { text: 'return value' }
});

export const acceptFriendInvite = functions.https.onCall(async (data, context) => {

    //check preconditions: Authorization
    checkAuth(context)

    const dbRoot = data.dbRoot
    let myUid = ""
    
    if(context.auth === undefined) {
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
        
    } else {
        myUid = context.auth.uid;
    }
    const fuid = data.friendID

    //read current friend status to ensure the invite is allowed:
    //   NOT: unfriend received or invite rejected or invite sent
    const currentStatus = await getCuurentStatus(myUid, fuid, dbRoot)

    if (currentStatus === sInviteReceived) {
        await acceptInvite(myUid, fuid, dbRoot)
    }
    else {
        console.log("That person has not invited you")
        throw new functions.https.HttpsError('failed-precondition', 'That person has not invited you');
    }
    return { text: 'return value' }
});

async function acceptInvite(myUid: string, fuid: string, baseDBPath: string) {
    //get the users name
    const userName = await getUserName(myUid, baseDBPath)
    console.log('userName: ' + userName)

    //send FCM
    const message =
    {
        token: "",
        notification: {
            title: "New Friend",
            body: userName + ' has accepted your friend request'
        },
        "apns": {
            "headers":{

                "apns-priority": "10",
            },
            "payload":{
                "aps":{
                    "sound": "default",
                    "badge": 1
                }
            }
        },
        data: {
            action: CHANGE_FRIEND_STATUS
        }
    }

    await sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)
    //see if there is metadata already created, if not create it

    //first create the chatID
    const chatID = createFriendChatID(myUid, fuid)

    //get the chatMeta
    const chatMeta = await getFriendChatMeta(chatID, baseDBPath)
    console.log('chatMeta ', chatMeta)

    //if it doesn't exist, create it
    if (chatMeta === "") {
        await createFriendChatMeta(myUid, fuid, chatID, baseDBPath)
    }

    return updateFriendStatus(myUid, fuid, sFriend, sFriend, baseDBPath);
}

async function sendInvite(myUid: string, fuid: string, baseDBPath: string) {
    //get the users name
    const userName = await getUserName(myUid, baseDBPath)
    console.log('userName: ' + userName)

    
    //////////////////////////
    const message =
        {
            token: "",
            notification: {
                title: "New Friend Request",
                body: userName + ' invited you to be friends'
            },
            "apns": {
                "headers":{
    
                    "apns-priority": "10",
                },
                "payload":{
                    "aps":{
                        "sound": "default",
                        "badge": 1
                    }
                }
            },
            data: {
                action: CHANGE_FRIEND_STATUS
            }
        }
    //////////////////////////
    //send FCM
    // const message: admin.messaging.MessagingPayload =
    // {
    //     // token: "",
    //     notification: {
    //         title: "New Friend Request",
    //         body: userName + ' invited you to be friends'
    //     },
    //     "apns": {
    //         "headers":{

    //             "apns-priority": "10",
    //         },
    //         "payload":{
    //             "aps":{
    //                 "sound": "default",
    //                 "badge": 1
    //             }
    //         }
    //     },
    //     data: {
    //         action: CHANGE_FRIEND_STATUS
    //     }
    // }

    

    await sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)
    return updateFriendStatus(myUid, fuid, sInviteSent, sInviteReceived, baseDBPath);
}

async function sendFCMNotification(toUid: string, message: admin.messaging.MessagingPayload, baseDBPath: string, notificationType: string) {
    //load FCM token
    //devDB/FCMTokens/LPqSbpRiXWTW2rryqliKXtbxwz83
    const userPath = baseDBPath + '/FCMTokens/' + toUid
    // console.log("FCM Path: " + userPath)
    const snapshot = await admin.database().ref(userPath).once("value")
    const FCMTokens: string[] = []
    //if (snapshot.exists) {
    if(snapshot.val() !== null){
        snapshot.forEach((snap) => {
            //iterate through them getting the profile of each friend, adding to an array
            // console.log("User " + snap.key + " status is " + snap.val());
            const device = snap.val();
            // console.log('device ', device)
            if(device[notificationType] === true)
            {
                // console.log(notificationType, ' is true')
                FCMTokens.push(device.FCMToken)
            }
            else {
                // console.log(notificationType, ' is false')
            }
            return false;
        });
    }    
    else {
        console.log("snapshot doesn't exist -> no FCM tokens")
        return
    }
    // console.log("FCM Tokens: ", FCMTokens)

    //create data
    // message.token = FCMToken
    console.log("Message: ", message)

    //send notification
    console.log("Message: ", message)
    admin.messaging().sendToDevice(FCMTokens, message)
    // admin.messaging().send(message)
    .then((response) => {
        // Response is a message ID string.
        console.log('Successfully sent message to:', FCMTokens, response);
    })
    .catch((error) => {
        console.log('Error sending message to:', FCMTokens, error);
    });


    // Send a message to the devices corresponding to the provided registration tokens.
    // FCMTokens.forEach((FCMToken) => {
    //     message.token = FCMToken
    //     console.log("Message: ", message)
    //     admin.messaging().sendToDevice()
    //     // admin.messaging().send(message)
    //     .then((response) => {
    //         // Response is a message ID string.
    //         console.log('Successfully sent message to:', FCMToken, response);
    //     })
    //     .catch((error) => {
    //         console.log('Error sending message to:', FCMToken, error);
    //     });

    // });
}

async function updateFriendStatus(myUid, fuid, myNewStatus, friendNewStatus, baseDBPath) {
    // this will write the new statuses to the correct userIDs
    // Database.getInstance().getDBPath("/Users/" + fromUID + "/FriendsList/" + toUID);
    const userPath = baseDBPath + '/Users/' + myUid + '/FriendsList/' + fuid
    console.log("userPath: " + userPath)

    const friendPath = baseDBPath + '/Users/' + fuid + '/FriendsList/' + myUid
    console.log("friendPath: " + friendPath)

    //write to the Database
    const db = admin.database();
    const userRef = db.ref(userPath);
    const friendRef = db.ref(friendPath)
    await userRef.set(myNewStatus)
    return friendRef.set(friendNewStatus)
}

function checkAuth(context: functions.https.CallableContext) {
    if (context.auth === undefined || !context.auth) {
        // Throwing an HttpsError so that the client gets the error details.
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
    }
}

async function getUserName(uid, baseDBPath) {
    ///Users/x5oBAIOj1KU3gKVFjl5EAAOPhlh2/User/publicData/displayName
    const userNamePath = baseDBPath + '/Users/' + uid + '/Profile/displayName'
    // const userNamePath = baseDBPath + '/Users/x5oBAIOj1KU3gKVFjl5EAAOPhlh2/User/publicData/displayName'
    console.log("User name path: " + userNamePath)
    const snap = await admin.database().ref(userNamePath).once("value")
    if (snap.exists()) {
        return snap.val()
    }
    else {
        console.log("userName snapshot does not exist")
        return ""
    }
}

async function getCuurentStatus(myUid, fuid, baseDBPath) {
    //
    const userPath = baseDBPath + '/Users/' + myUid + '/FriendsList/' + fuid
    // console.log("Path: " + userPath)
    const snap = await admin.database().ref(userPath).once("value")
    let toReturn
    if (snap.exists()) {
        toReturn = snap.val()
    }
    else {
        console.log("status snapshot does not exist")
        toReturn = ""
    }
    // console.log("currentStatus: ", toReturn)
    return toReturn
}

//--------------------------------- profile Functions --------------------------------------------------------
//--------------------------------- profile Functions --------------------------------------------------------
//--------------------------------- profile Functions --------------------------------------------------------

export const getFriendProfile = functions.https.onCall(async (data,  context:any) => {

    //check preconditions: Authorization
    checkAuth( context)
   

    const baseDBPath = data.dbRoot
    const myUid = context.auth.uid;
  //  console.log(myUid);
    const fuid = data.friendID

    console.log(myUid + " is requesting getFriendProfile")

    //get current status
    const snap = await getCuurentStatus(myUid, fuid, baseDBPath)

    if (snap !== sInviteRejected && snap !== sUnfriendReceived) {
        //We are friends so we can load the profile
        const profile = await getProfile(fuid, baseDBPath)

        console.log('returning profile: ' + profile)
        return JSON.stringify(profile)
    }
    else {
        console.log("You are not friends with that person")
        throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
    }
});

export const getAllFriendsProfiles = functions.https.onCall(async (data,  context:any) => {

    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    const myUid = context.auth.uid;

    console.log(myUid + " is requesting AllFriendsProfiles")

    //first get all friend statuses
    const friends = [];

    const profiles = new Object();

    const friendsListPath = baseDBPath + '/Users/' + myUid + '/FriendsList'
    const snapshot = await loadValueFromDB(friendsListPath)
   // if (snapshot.exists) {
    if(snapshot.val() !== null){
        snapshot.forEach((snap) => {
            //iterate through them getting the profile of each friend, adding to an array
            // console.log("User " + snap.key + " status is " + snap.val());
            if (snap.val() !== sInviteRejected && snap.val() !== sUnfriendReceived) {
               // const snap : string[] = [];
               //this.friends.push({ snap key });
                friends.push(snap.key)
            }
            return false;
        });

        //here the friends array has all our ids
        for (const friendID of friends) {
            const profile = await getProfile(friendID, baseDBPath)
            // console.log("ret prof", profile)
            if (profile !== "") {
                 //friendID: any;
                // console.log("adding Profile", "from " + friendID + ": ", profile)
                profiles[friendID] = profile
                // profiles.set(friendID, profile)
            }
        }

    }
    console.log("returning map of " + Object.keys(profiles).length + " profiles: ", profiles)

    // return profiles
    return JSON.stringify(profiles)
});

async function getProfile(uid, baseDBPath) {
    //get the user profile of the uid, in the correct database
    const profilePath = baseDBPath + '/Users/' + uid + '/Profile'
    // console.log("Path: " + profilePath)
    const snap = await admin.database().ref(profilePath).once("value")
    let toReturn
    if (snap.exists()) {
        toReturn = snap.val()
    }
    else {
        // console.log("profile snapshot does not exist")
        toReturn = ""
    }
    // console.log("profile: ", toReturn)
    return toReturn
}

async function loadValueFromDB(path) {
    const snap = await admin.database().ref(path).once("value")
    return snap
}

async function updateValueToDB(data, path) {
    return admin.database().ref(path).update(data)
    // return snap
}


//--------------------------------- Chat Functions --------------------------------------------------------
//--------------------------------- Chat Functions --------------------------------------------------------
//--------------------------------- Chat Functions --------------------------------------------------------

export const sendChatToFriend = functions.https.onCall(async (data,  context:any) => {

    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    const myUid = context.auth.uid;
    const fuid = data.friendID
    // const chatMessage = JSON.parse(data.message)
    const chatMessage = data.message
    /*
    private String fromUid;
    private String msgText;
    private long timeStamp;
    private int number;
    */

    //get current status
    const currentStatus = await getCuurentStatus(myUid, fuid, baseDBPath)

    if (currentStatus === sFriend) {
        //We are friends so we can send the chat message

        //see if there is metadata already created, if not create it

        //first create the chatID
        const friendChatID = createFriendChatID(myUid, fuid)

        //get the chatMeta
        // let chatMeta = await getFriendChatMeta(chatID, baseDBPath)
        // // console.log('chatMeta ' , chatMeta)

        // //if it doesn't exist, create it
        // if (chatMeta === "") {
        //     chatMeta = await createFriendChatMeta(myUid, fuid, chatID, baseDBPath)
        // }

        //get the next chat message number
        // chatMessage.number = chatMeta.latestMessageNumber++

        //write the message to the friendChat
        const latestMessagePath = baseDBPath + '/FriendChats/' + friendChatID + '/latestMessageNumber'
        const nextNumber = await incrementNumberAtPath(latestMessagePath)
        // console.log('next number', nextNumber, ' type ', typeof nextNumber, ' obj type: ', typeof chatMessage.number)
        chatMessage.number = nextNumber

        // console.log('path ', nextMessagePath)
        chatMessage.timeStamp = admin.database.ServerValue.TIMESTAMP
        chatMessage.numberOfViews = 0
        console.log('writing chatMessage ', chatMessage)
        const nextMessagePath = baseDBPath + '/FriendChats/' + friendChatID + '/chatMeta/latestMessage'
        const messagePath = baseDBPath + '/FriendChats/' + friendChatID + '/messages/' + chatMessage.number
        await updateValueToDB(chatMessage, messagePath)
        await updateValueToDB(chatMessage, nextMessagePath)

        //send the FCM to the friend
        const friendUserName = await getUserName(myUid, baseDBPath)
        //send FCM
        const message =
        {
            token: "",
            notification: {
                title: friendUserName,
                body: chatMessage['msgText']
            },
            "apns": {
                "headers":{
    
                    "apns-priority": "10",
                },
                "payload":{
                    "aps":{
                        "sound": "default",
                        "badge": 1
                    }
                }
            },
            data: {
                action: NEW_FRIEND_CHAT_MESSAGE,
                chatID: friendChatID
            }
        }
        await sendFCMNotification(fuid, message, baseDBPath, sFriendChatNotify)
        //return

    }
    else {
        console.log("You are not friends with that person")
        throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
    }
    return 'success'
});


export const joinFriendChat = functions.https.onCall(async (data,  context:any) => {

    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    const myUid = context.auth.uid;
    const fuid = data.friendID
    const chatMember = data.chatMember;
    console.log('chatMember', chatMember)

    const currentStatus = await getCuurentStatus(myUid, fuid, baseDBPath)

    if (currentStatus === sFriend) {
        //first create the chatID
        const chatID = createFriendChatID(myUid, fuid)

        //write the chatRoomMember to the metaData
        const myChatMemberPath = baseDBPath + '/FriendChats/' + chatID + '/chatMeta/chatRoomMembers/' + myUid
        console.log('path', myChatMemberPath)
        await updateValueToDB(chatMember, myChatMemberPath);
    }
    else {
        console.log("You are not friends with that person")
        throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
    }
    return 'success'
});


export const clearFriendChatOfAllMessages = functions.https.onCall(async (data, context:any) => {

    //data sent from App:
    /*
        data["friendID"] = fuid
        data["chatID"] = chatID    */
    //check preconditions: Authorization
    checkAuth(context)

    const baseDBPath = data.dbRoot
    const myUid = context.auth.uid;
    const fuid = data.friendID

    //get current status
    const currentStatus = await getCuurentStatus(myUid, fuid, baseDBPath)

    if(currentStatus !== sFriend) {
        console.log("You are not friends with that person")
        throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
        // return 'failed'
    }
    //first create the chatID
    const friendChatID = createFriendChatID(myUid, fuid)

    if(friendChatID === "") {
        //this could delete EVERYTHING ... don't do it
        console.log("friendChatID cannot be blank")
        throw new functions.https.HttpsError('failed-precondition', 'blank chat id');
        // return 'failed'
    }

    //get the chatMeta
    const chatMeta = await getFriendChatMeta(friendChatID, baseDBPath)
    console.log('original meta', chatMeta)

    //update revision and latestChatNumber
    const d = new Date()
    chatMeta.chatRevision = Math.round(d.getTime() / 1000)

    chatMeta.latestMessage = null

    //delete all at chat path
    const chatPath = baseDBPath + '/FriendChats/' + friendChatID

    console.log('deleting friend chat', chatPath)
    await admin.database().ref(chatPath).remove()

    //save updated meta
    console.log('saving new meta', chatMeta)
    const metaPath = baseDBPath + '/FriendChats/' + friendChatID + '/chatMeta'
    await updateValueToDB(chatMeta, metaPath);

    //return success
    return 'success'
});


async function incrementNumberAtPath(path) {
    let newNumber
    const ref = admin.database().ref(path)
    await ref.transaction(function (currentNumber) {
        // If users/ada/rank has never been set, currentRank will be `null`.
        if (currentNumber === null) {
            console.log("increment number is null")
            newNumber = 1
            return newNumber
        }
        console.log("incrementing old number: ")
        newNumber = parseInt(currentNumber) + 1
        console.log("increment old number: ", currentNumber, ' new:', newNumber)
        return newNumber;
    });
    console.log('returning ', newNumber)
    return newNumber
}

function createFriendChatID(myUid, fuid) {
    //create the id
    let chatID

    if (myUid < fuid) {

        chatID = myUid + '_' + fuid;
    }
    else {
        chatID = fuid + '_' + myUid;
    }
    console.log('chatID: ' + chatID)
    return chatID
}

async function createFriendChatMeta(myUid, fuid, friendChatID, baseDBPath) {
    /*
    JAVA chatMeta class:
    
    private Map<String, ChatMember> chatRoomMembers;
    private String chatID;
    private String chatTitle;
    private ChatMessage latestMessage;
    private int latestMessageNumber;
    */
    const metaPath = baseDBPath + '/FriendChats/' + friendChatID + '/chatMeta'
    const newMeta = new Object();
    newMeta['chatID'] = friendChatID;
    newMeta['chatTitle'] = 'friend Chat'
    // newMeta['latestMessageNumber'] = 0
    console.log('writing ', newMeta + ' to path: ' + metaPath)
    await updateValueToDB(newMeta, metaPath)
    return newMeta
}

async function getFriendChatMeta(chatID, baseDBPath) {

    //get the user profile of the uid, in the correct database
    const metaPath = baseDBPath + '/FriendChats/' + chatID + '/chatMeta'
    console.log("Path: " + metaPath)
    const snap = await loadValueFromDB(metaPath)
    let toReturn
    if (snap.exists()) {
        toReturn = snap.val()
    }
    else {
        // console.log("profile snapshot does not exist")
        toReturn = ""
    }
    // console.log("profile: ", toReturn)
    return toReturn
}

export const joinGroupChat = functions.https.onCall(async (data, context:any) => {

    //check preconditions: Authorization
    checkAuth(context)

    const myUid = context.auth.uid;
    const chatPath = data.path
    const chatMember = data.chatMember;
    const chatID = data.chatID;
    const chatTitle = data.chatTitle

    console.log('chatMember', chatMember)

    //get the chatMeta
    const chatMeta = await getGroupChatMeta(chatPath)
    console.log('chatMeta ', chatMeta)

    //if it doesn't exist, create it
    if (chatMeta === "") {
        console.log('creating group chat meta', chatPath)
        await createGroupChatMeta(chatID, chatPath, chatTitle, myUid, chatMember)
    }
    else
    {
        const membersPath = chatPath + '/chatMeta/chatRoomMembers/' + myUid
        console.log('joining group chat meta', membersPath)
        await updateValueToDB(chatMember, membersPath);
    }

    //write a chatMessage for joining
    // const chatMessage = {
    //     fromUid: myUid,
    //     msgText: chatMember.displayName +  ' joined the chat',
    // }
    // await writeGroupChatMessage(chatPath, chatMessage);

    return 'success'
});

async function getGroupChatMeta(path) {

    //get the user profile of the uid, in the correct database
    const metaPath = path + '/chatMeta'
    console.log("Path: " + metaPath)
    const snap = await loadValueFromDB(metaPath)
    let toReturn
    if (snap.exists()) {
        toReturn = snap.val()
    }
    else {
        // console.log("profile snapshot does not exist")
        toReturn = ""
    }
    // console.log("profile: ", toReturn)
    return toReturn
}

async function createGroupChatMeta(chatID, path, chatTitle, uid, chatMember) {
    /*
    JAVA chatMeta class:
    
    private Map<String, ChatMember> chatRoomMembers;
    private String chatID;
    private String chatTitle;
    private ChatMessage latestMessage;
    private int latestMessageNumber;
    */
    const metaPath = path + '/chatMeta'
    const chatMemberPath = '/chatRoomMembers/' + uid
    const newMeta = new Object();
    newMeta['chatID'] = chatID
    newMeta['chatTitle'] = chatTitle
    //update revision and latestChatNumber
    const d = new Date()
    newMeta['chatRevision'] = Math.round(d.getTime() / 1000)

    // newMeta['latestMessageNumber'] = 0
    newMeta[chatMemberPath] = chatMember
    console.log('creating META ', newMeta + ' to path: ' + metaPath)
    await updateValueToDB(newMeta, metaPath)
    return newMeta
}

export const leaveGroupChat = functions.https.onCall(async (data, context:any) => {

    //check preconditions: Authorization
    checkAuth(context)

    const myUid = context.auth.uid;
    const chatPath = data.path

    const membersPath = chatPath + '/chatMeta/chatRoomMembers/' + myUid
    const snapshot = await loadValueFromDB(membersPath)

    console.log('deleting member from group chat meta', membersPath)
    await admin.database().ref(membersPath).remove()


//     if(snapshot.exists())
//     {
//         const chatMember = snapshot.val()
//         //write a chatMessage for leaving
//         const chatMessage = {
//             fromUid: myUid,
//             msgText: chatMember.displayName +  ' left the chat',
//     }
//     await writeGroupChatMessage(chatPath, chatMessage);
// }
    

    return 'success'
});

export const sendChatToGroup = functions.https.onCall(async (data, context:any ) => {

    //check preconditions: Authorization
    checkAuth(context)

    const myUid = context.auth.uid;
    const chatPath = data.path
    const chatMessage = data.message
    const chatID = data.chatID
    const chatType = data.chatType
    const chatTitle = data.chatTitle
    /*
    private String fromUid;
    private String msgText;
    private long timeStamp;
    private int number;
    */

   await writeGroupChatMessage(chatPath, chatMessage);

   //send the FCM to the group
   //get the chatMeta
   const chatMeta = await getGroupChatMeta(chatPath)
   const chatRoomMembers = chatMeta.chatRoomMembers
   const senderUserName = chatRoomMembers[myUid].displayName
   let notifyType
   switch(chatType)
   {
       case sFlightType:
            notifyType = sFlightChatNotify
            break;
            
       case sLayoverType:
       default:

           notifyType = sLayoverChatNotify
           break;
   }
   console.log('chatNotifyType ', notifyType)

   const body =  senderUserName + ': ' + chatMessage['msgText']
   const message =
   {
       token: "",
       notification: {
           title: chatTitle,
           body: body
       },
       "apns": {
        "headers":{

            "apns-priority": "10",
        },
        "payload":{
            "aps":{
                "sound": "default",
                "badge": 1
            }
        }
    },
       data: {
           action: NEW_GROUP_CHAT_MESSAGE,
           chatID: chatID
       }
   }
   const baseDBPath = data.dbRoot

   console.log('chatmembers ', chatRoomMembers)
   console.log('chatmembers type', typeof chatRoomMembers)

 
    for (const i in chatRoomMembers) {

    const element = chatRoomMembers[i]
    console.log('element ', element)

    const notify = element.sendNotifications
    if(!notify || element.uid === myUid) {
        console.log('skipping element ', element)
        continue
    }
    const uid = element.uid;
    //send FCM
    await sendFCMNotification(uid, message, baseDBPath, notifyType)

   };

    return 'success'
});

async function writeGroupChatMessage(chatPath: any, chatMessage: any) {
    const latestMessagePath = chatPath + '/latestMessageNumber';
    const nextNumber = await incrementNumberAtPath(latestMessagePath);
    // console.log('next number', nextNumber, ' type ', typeof nextNumber, ' obj type: ', typeof chatMessage.number)
    chatMessage.number = nextNumber;
    // console.log('path ', nextMessagePath)
    chatMessage.timeStamp = admin.database.ServerValue.TIMESTAMP;
    chatMessage.numberOfViews = 0;
    console.log('writing groupChatMessage ', chatMessage);
    const nextMessagePath = chatPath + '/chatMeta/latestMessage';
    const messagePath = chatPath + '/messages/' + chatMessage.number;
    await updateValueToDB(chatMessage, messagePath);
    await updateValueToDB(chatMessage, nextMessagePath);
}
