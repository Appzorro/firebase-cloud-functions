"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.sendChatToGroup = exports.leaveGroupChat = exports.joinGroupChat = exports.clearFriendChatOfAllMessages = exports.joinFriendChat = exports.sendChatToFriend = exports.getAllFriendsProfiles = exports.getFriendProfile = exports.acceptFriendInvite = exports.rejectFriendInvite = exports.unFriend = exports.removeUnfriended = exports.cancelFriendInvite = exports.sendFriendInvite = void 0;
var functions = require("firebase-functions");
var admin = require("firebase-admin");
//import { UserDimensions } from 'firebase-functions/lib/providers/analytics';
//import { DataSnapshot } from 'firebase-functions/lib/providers/database';
//constants that describe the state of a friend invite
var sInviteSent = "invite sent";
var sInviteReceived = "invited";
var sInviteRejected = "invite rejected";
var sRejectedInvite = "rejected invite";
var sFriend = "friend";
var sUnfriendSent = "unfriended";
var sUnfriendReceived = "unfriend received";
var sInviteCancel = "cancelled invite";
//FCM action constants for Android
var CHANGE_FRIEND_STATUS = "com.crewconnect.CHANGE_FRIEND_STATUS";
var NEW_FRIEND_CHAT_MESSAGE = "com.crewconnect.NEW_FRIEND_CHAT_MESSAGE";
var NEW_GROUP_CHAT_MESSAGE = "com.crewconnect.NEW_GROUP_CHAT_MESSAGE";
//constants for what type of notification should(or shouldn't) be sent
var sFriendChatNotify = "friendChatNotify";
var sFlightChatNotify = "flightChatNotify";
var sLayoverChatNotify = "layoverChatNotify";
var sFriendStatusNotify = "friendStatusNotify";
//constants for type of group chat
var sFlightType = "flight";
var sLayoverType = "layover";
//possible friend actions:
//  invite friend, cancel invite, accept invite, reject invite, unfriend, 
//console.log('authorization: ', context.auth)
console.log('hy');
/////////////////////////////
// admin.initializeApp({
//     credential: admin.credential.applicationDefault(),
//     databaseURL: 'https://crewconnect-na.firebaseio.com'
//   });
////////////////////////////
//--------------------------------- friendStatus Functions --------------------------------------------------------
//--------------------------------- friendStatus Functions --------------------------------------------------------
//--------------------------------- friendStatus Functions --------------------------------------------------------
exports.sendFriendInvite = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var dbRoot, myUid, fuid, currentStatus, _a;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                dbRoot = data.dbRoot;
                myUid = "";
                if (context.auth === undefined) {
                    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
                        'while authenticated.');
                }
                else {
                    myUid = context.auth.uid;
                }
                fuid = data.friendID;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, dbRoot)];
            case 1:
                currentStatus = _b.sent();
                _a = currentStatus;
                switch (_a) {
                    case sInviteReceived: return [3 /*break*/, 2];
                    case sInviteSent: return [3 /*break*/, 4];
                    case sInviteRejected: return [3 /*break*/, 5];
                    case sFriend: return [3 /*break*/, 6];
                    case sUnfriendReceived: return [3 /*break*/, 7];
                    case sInviteCancel: return [3 /*break*/, 8];
                    case sUnfriendSent: return [3 /*break*/, 8];
                    case sRejectedInvite: return [3 /*break*/, 8];
                    case "": return [3 /*break*/, 8];
                }
                return [3 /*break*/, 9];
            case 2:
                //accept invite
                console.log("accept invite");
                return [4 /*yield*/, acceptInvite(myUid, fuid, dbRoot)];
            case 3:
                _b.sent();
                return [3 /*break*/, 9];
            case 4:
                console.log("You've already invited that person");
                throw new functions.https.HttpsError('failed-precondition', 'You\'ve already invited that person');
            case 5:
                console.log("That person has rejected an invite from you");
                throw new functions.https.HttpsError('failed-precondition', 'That person has rejected an invite from you');
            case 6:
                console.log("You are already friends with that person");
                throw new functions.https.HttpsError('failed-precondition', 'You are already friends with that person');
            case 7:
                console.log("That person has unfriended you and you cannot send an invite");
                throw new functions.https.HttpsError('failed-precondition', 'That person has unfriended you and you cannot send an invite');
            case 8:
                console.log("send invite");
                return [2 /*return*/, sendInvite(myUid, fuid, dbRoot)];
            case 9: return [2 /*return*/, { text: 'return value' }];
        }
    });
}); });
exports.cancelFriendInvite = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, currentStatus, userName, message;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = "";
                if (context.auth === undefined) {
                    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
                        'while authenticated.');
                }
                else {
                    myUid = context.auth.uid;
                }
                fuid = data.friendID;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                currentStatus = _a.sent();
                if (!(currentStatus === sInviteSent)) return [3 /*break*/, 5];
                return [4 /*yield*/, getUserName(myUid, baseDBPath)];
            case 2:
                userName = _a.sent();
                console.log('userName: ' + userName);
                message = {
                    token: "",
                    notification: {
                        title: "Friend Invite Cancelled",
                        body: userName + ' has cancelled their invitation to be friends'
                    },
                    "apns": {
                        "headers": {
                            "apns-priority": "10"
                        },
                        "payload": {
                            "aps": {
                                "sound": "default",
                                "badge": 1
                            }
                        }
                    },
                    data: {
                        action: CHANGE_FRIEND_STATUS
                    }
                };
                return [4 /*yield*/, sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)];
            case 3:
                _a.sent();
                return [4 /*yield*/, updateFriendStatus(myUid, fuid, sInviteCancel, sInviteCancel, baseDBPath)];
            case 4:
                _a.sent();
                return [3 /*break*/, 6];
            case 5:
                console.log("You have not invited that person");
                throw new functions.https.HttpsError('failed-precondition', 'You have not invited that person');
            case 6: return [2 /*return*/, { text: 'return value' }];
        }
    });
}); });
exports.removeUnfriended = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, currentStatus, userPath, friendPath, db, userRef, friendRef;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = "";
                if (context.auth === undefined) {
                    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
                        'while authenticated.');
                }
                else {
                    myUid = context.auth.uid;
                }
                fuid = data.friendID;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                currentStatus = _a.sent();
                if (!(currentStatus === sUnfriendSent || currentStatus === sUnfriendReceived)) return [3 /*break*/, 3];
                userPath = baseDBPath + '/Users/' + myUid + '/FriendsList/' + fuid;
                console.log("userPath: " + userPath);
                friendPath = baseDBPath + '/Users/' + fuid + '/FriendsList/' + myUid;
                console.log("friendPath: " + friendPath);
                db = admin.database();
                userRef = db.ref(userPath);
                friendRef = db.ref(friendPath);
                return [4 /*yield*/, userRef.remove()];
            case 2:
                _a.sent();
                return [2 /*return*/, friendRef.remove()];
            case 3:
                console.log("You have not unfriended that person");
                throw new functions.https.HttpsError('failed-precondition', 'You have not unfriended that person');
        }
    });
}); });
exports.unFriend = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, currentStatus, userName, message;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = "";
                if (context.auth === undefined) {
                    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
                        'while authenticated.');
                }
                else {
                    myUid = context.auth.uid;
                }
                fuid = data.friendID;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                currentStatus = _a.sent();
                if (!(currentStatus === sFriend)) return [3 /*break*/, 5];
                return [4 /*yield*/, getUserName(myUid, baseDBPath)];
            case 2:
                userName = _a.sent();
                console.log('userName: ' + userName);
                message = {
                    token: "",
                    notification: {
                        title: "Unfriended",
                        body: userName + ' is no longer friends with you'
                    },
                    "apns": {
                        "headers": {
                            "apns-priority": "10"
                        },
                        "payload": {
                            "aps": {
                                "sound": "default",
                                "badge": 1
                            }
                        }
                    },
                    data: {
                        action: CHANGE_FRIEND_STATUS
                    }
                };
                return [4 /*yield*/, sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)];
            case 3:
                _a.sent();
                return [4 /*yield*/, updateFriendStatus(myUid, fuid, sUnfriendSent, sUnfriendReceived, baseDBPath)];
            case 4:
                _a.sent();
                return [3 /*break*/, 6];
            case 5:
                console.log("Your are not friends with that person");
                throw new functions.https.HttpsError('failed-precondition', 'Your are not friends with that person');
            case 6: return [2 /*return*/, { text: 'return value' }];
        }
    });
}); });
exports.rejectFriendInvite = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, currentStatus, userName, message;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = "";
                if (context.auth === undefined) {
                    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
                        'while authenticated.');
                }
                else {
                    myUid = context.auth.uid;
                }
                fuid = data.friendID;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                currentStatus = _a.sent();
                if (!(currentStatus === sInviteReceived)) return [3 /*break*/, 5];
                return [4 /*yield*/, getUserName(myUid, baseDBPath)];
            case 2:
                userName = _a.sent();
                console.log('userName: ' + userName);
                message = {
                    token: "",
                    notification: {
                        title: "Declined Friend Invite",
                        body: userName + ' has declined your friend request'
                    },
                    "apns": {
                        "headers": {
                            "apns-priority": "10"
                        },
                        "payload": {
                            "aps": {
                                "sound": "default",
                                "badge": 1
                            }
                        }
                    },
                    data: {
                        action: CHANGE_FRIEND_STATUS
                    }
                };
                return [4 /*yield*/, sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)];
            case 3:
                _a.sent();
                return [4 /*yield*/, updateFriendStatus(myUid, fuid, sRejectedInvite, sInviteRejected, baseDBPath)];
            case 4:
                _a.sent();
                return [3 /*break*/, 6];
            case 5:
                console.log("That person has not invited you");
                throw new functions.https.HttpsError('failed-precondition', 'That person has not invited you');
            case 6: return [2 /*return*/, { text: 'return value' }];
        }
    });
}); });
exports.acceptFriendInvite = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var dbRoot, myUid, fuid, currentStatus;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                dbRoot = data.dbRoot;
                myUid = "";
                if (context.auth === undefined) {
                    throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
                        'while authenticated.');
                }
                else {
                    myUid = context.auth.uid;
                }
                fuid = data.friendID;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, dbRoot)];
            case 1:
                currentStatus = _a.sent();
                if (!(currentStatus === sInviteReceived)) return [3 /*break*/, 3];
                return [4 /*yield*/, acceptInvite(myUid, fuid, dbRoot)];
            case 2:
                _a.sent();
                return [3 /*break*/, 4];
            case 3:
                console.log("That person has not invited you");
                throw new functions.https.HttpsError('failed-precondition', 'That person has not invited you');
            case 4: return [2 /*return*/, { text: 'return value' }];
        }
    });
}); });
function acceptInvite(myUid, fuid, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var userName, message, chatID, chatMeta;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getUserName(myUid, baseDBPath)];
                case 1:
                    userName = _a.sent();
                    console.log('userName: ' + userName);
                    message = {
                        token: "",
                        notification: {
                            title: "New Friend",
                            body: userName + ' has accepted your friend request'
                        },
                        "apns": {
                            "headers": {
                                "apns-priority": "10"
                            },
                            "payload": {
                                "aps": {
                                    "sound": "default",
                                    "badge": 1
                                }
                            }
                        },
                        data: {
                            action: CHANGE_FRIEND_STATUS
                        }
                    };
                    return [4 /*yield*/, sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)
                        //see if there is metadata already created, if not create it
                        //first create the chatID
                    ];
                case 2:
                    _a.sent();
                    chatID = createFriendChatID(myUid, fuid);
                    return [4 /*yield*/, getFriendChatMeta(chatID, baseDBPath)];
                case 3:
                    chatMeta = _a.sent();
                    console.log('chatMeta ', chatMeta);
                    if (!(chatMeta === "")) return [3 /*break*/, 5];
                    return [4 /*yield*/, createFriendChatMeta(myUid, fuid, chatID, baseDBPath)];
                case 4:
                    _a.sent();
                    _a.label = 5;
                case 5: return [2 /*return*/, updateFriendStatus(myUid, fuid, sFriend, sFriend, baseDBPath)];
            }
        });
    });
}
function sendInvite(myUid, fuid, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var userName, message;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, getUserName(myUid, baseDBPath)];
                case 1:
                    userName = _a.sent();
                    console.log('userName: ' + userName);
                    message = {
                        token: "",
                        notification: {
                            title: "New Friend Request",
                            body: userName + ' invited you to be friends'
                        },
                        "apns": {
                            "headers": {
                                "apns-priority": "10"
                            },
                            "payload": {
                                "aps": {
                                    "sound": "default",
                                    "badge": 1
                                }
                            }
                        },
                        data: {
                            action: CHANGE_FRIEND_STATUS
                        }
                    };
                    //////////////////////////
                    //send FCM
                    // const message: admin.messaging.MessagingPayload =
                    // {
                    //     // token: "",
                    //     notification: {
                    //         title: "New Friend Request",
                    //         body: userName + ' invited you to be friends'
                    //     },
                    //     "apns": {
                    //         "headers":{
                    //             "apns-priority": "10",
                    //         },
                    //         "payload":{
                    //             "aps":{
                    //                 "sound": "default",
                    //                 "badge": 1
                    //             }
                    //         }
                    //     },
                    //     data: {
                    //         action: CHANGE_FRIEND_STATUS
                    //     }
                    // }
                    return [4 /*yield*/, sendFCMNotification(fuid, message, baseDBPath, sFriendStatusNotify)];
                case 2:
                    //////////////////////////
                    //send FCM
                    // const message: admin.messaging.MessagingPayload =
                    // {
                    //     // token: "",
                    //     notification: {
                    //         title: "New Friend Request",
                    //         body: userName + ' invited you to be friends'
                    //     },
                    //     "apns": {
                    //         "headers":{
                    //             "apns-priority": "10",
                    //         },
                    //         "payload":{
                    //             "aps":{
                    //                 "sound": "default",
                    //                 "badge": 1
                    //             }
                    //         }
                    //     },
                    //     data: {
                    //         action: CHANGE_FRIEND_STATUS
                    //     }
                    // }
                    _a.sent();
                    return [2 /*return*/, updateFriendStatus(myUid, fuid, sInviteSent, sInviteReceived, baseDBPath)];
            }
        });
    });
}
function sendFCMNotification(toUid, message, baseDBPath, notificationType) {
    return __awaiter(this, void 0, void 0, function () {
        var userPath, snapshot, FCMTokens;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userPath = baseDBPath + '/FCMTokens/' + toUid;
                    return [4 /*yield*/, admin.database().ref(userPath).once("value")];
                case 1:
                    snapshot = _a.sent();
                    FCMTokens = [];
                    //if (snapshot.exists) {
                    if (snapshot.val() !== null) {
                        snapshot.forEach(function (snap) {
                            //iterate through them getting the profile of each friend, adding to an array
                            // console.log("User " + snap.key + " status is " + snap.val());
                            var device = snap.val();
                            // console.log('device ', device)
                            if (device[notificationType] === true) {
                                // console.log(notificationType, ' is true')
                                FCMTokens.push(device.FCMToken);
                            }
                            else {
                                // console.log(notificationType, ' is false')
                            }
                            return false;
                        });
                    }
                    else {
                        console.log("snapshot doesn't exist -> no FCM tokens");
                        return [2 /*return*/];
                    }
                    // console.log("FCM Tokens: ", FCMTokens)
                    //create data
                    // message.token = FCMToken
                    console.log("Message: ", message);
                    //send notification
                    console.log("Message: ", message);
                    admin.messaging().sendToDevice(FCMTokens, message)
                        // admin.messaging().send(message)
                        .then(function (response) {
                        // Response is a message ID string.
                        console.log('Successfully sent message to:', FCMTokens, response);
                    })["catch"](function (error) {
                        console.log('Error sending message to:', FCMTokens, error);
                    });
                    return [2 /*return*/];
            }
        });
    });
}
function updateFriendStatus(myUid, fuid, myNewStatus, friendNewStatus, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var userPath, friendPath, db, userRef, friendRef;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userPath = baseDBPath + '/Users/' + myUid + '/FriendsList/' + fuid;
                    console.log("userPath: " + userPath);
                    friendPath = baseDBPath + '/Users/' + fuid + '/FriendsList/' + myUid;
                    console.log("friendPath: " + friendPath);
                    db = admin.database();
                    userRef = db.ref(userPath);
                    friendRef = db.ref(friendPath);
                    return [4 /*yield*/, userRef.set(myNewStatus)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, friendRef.set(friendNewStatus)];
            }
        });
    });
}
function checkAuth(context) {
    if (context.auth === undefined || !context.auth) {
        // Throwing an HttpsError so that the client gets the error details.
        throw new functions.https.HttpsError('failed-precondition', 'The function must be called ' +
            'while authenticated.');
    }
}
function getUserName(uid, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var userNamePath, snap;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userNamePath = baseDBPath + '/Users/' + uid + '/Profile/displayName';
                    // const userNamePath = baseDBPath + '/Users/x5oBAIOj1KU3gKVFjl5EAAOPhlh2/User/publicData/displayName'
                    console.log("User name path: " + userNamePath);
                    return [4 /*yield*/, admin.database().ref(userNamePath).once("value")];
                case 1:
                    snap = _a.sent();
                    if (snap.exists()) {
                        return [2 /*return*/, snap.val()];
                    }
                    else {
                        console.log("userName snapshot does not exist");
                        return [2 /*return*/, ""];
                    }
                    return [2 /*return*/];
            }
        });
    });
}
function getCuurentStatus(myUid, fuid, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var userPath, snap, toReturn;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    userPath = baseDBPath + '/Users/' + myUid + '/FriendsList/' + fuid;
                    return [4 /*yield*/, admin.database().ref(userPath).once("value")];
                case 1:
                    snap = _a.sent();
                    if (snap.exists()) {
                        toReturn = snap.val();
                    }
                    else {
                        console.log("status snapshot does not exist");
                        toReturn = "";
                    }
                    // console.log("currentStatus: ", toReturn)
                    return [2 /*return*/, toReturn];
            }
        });
    });
}
//--------------------------------- profile Functions --------------------------------------------------------
//--------------------------------- profile Functions --------------------------------------------------------
//--------------------------------- profile Functions --------------------------------------------------------
exports.getFriendProfile = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, snap, profile;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = context.auth.uid;
                fuid = data.friendID;
                console.log(myUid + " is requesting getFriendProfile");
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                snap = _a.sent();
                if (!(snap !== sInviteRejected && snap !== sUnfriendReceived)) return [3 /*break*/, 3];
                return [4 /*yield*/, getProfile(fuid, baseDBPath)];
            case 2:
                profile = _a.sent();
                console.log('returning profile: ' + profile);
                return [2 /*return*/, JSON.stringify(profile)];
            case 3:
                console.log("You are not friends with that person");
                throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
        }
    });
}); });
exports.getAllFriendsProfiles = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, friends, profiles, friendsListPath, snapshot, _i, friends_1, friendID, profile;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = context.auth.uid;
                console.log(myUid + " is requesting AllFriendsProfiles");
                friends = [];
                profiles = new Object();
                friendsListPath = baseDBPath + '/Users/' + myUid + '/FriendsList';
                return [4 /*yield*/, loadValueFromDB(friendsListPath)
                    // if (snapshot.exists) {
                ];
            case 1:
                snapshot = _a.sent();
                if (!(snapshot.val() !== null)) return [3 /*break*/, 5];
                snapshot.forEach(function (snap) {
                    //iterate through them getting the profile of each friend, adding to an array
                    // console.log("User " + snap.key + " status is " + snap.val());
                    if (snap.val() !== sInviteRejected && snap.val() !== sUnfriendReceived) {
                        // const snap : string[] = [];
                        //this.friends.push({ snap key });
                        friends.push(snap.key);
                    }
                    return false;
                });
                _i = 0, friends_1 = friends;
                _a.label = 2;
            case 2:
                if (!(_i < friends_1.length)) return [3 /*break*/, 5];
                friendID = friends_1[_i];
                return [4 /*yield*/, getProfile(friendID, baseDBPath)
                    // console.log("ret prof", profile)
                ];
            case 3:
                profile = _a.sent();
                // console.log("ret prof", profile)
                if (profile !== "") {
                    //friendID: any;
                    // console.log("adding Profile", "from " + friendID + ": ", profile)
                    profiles[friendID] = profile;
                    // profiles.set(friendID, profile)
                }
                _a.label = 4;
            case 4:
                _i++;
                return [3 /*break*/, 2];
            case 5:
                console.log("returning map of " + Object.keys(profiles).length + " profiles: ", profiles);
                // return profiles
                return [2 /*return*/, JSON.stringify(profiles)];
        }
    });
}); });
function getProfile(uid, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var profilePath, snap, toReturn;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    profilePath = baseDBPath + '/Users/' + uid + '/Profile';
                    return [4 /*yield*/, admin.database().ref(profilePath).once("value")];
                case 1:
                    snap = _a.sent();
                    if (snap.exists()) {
                        toReturn = snap.val();
                    }
                    else {
                        // console.log("profile snapshot does not exist")
                        toReturn = "";
                    }
                    // console.log("profile: ", toReturn)
                    return [2 /*return*/, toReturn];
            }
        });
    });
}
function loadValueFromDB(path) {
    return __awaiter(this, void 0, void 0, function () {
        var snap;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, admin.database().ref(path).once("value")];
                case 1:
                    snap = _a.sent();
                    return [2 /*return*/, snap];
            }
        });
    });
}
function updateValueToDB(data, path) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, admin.database().ref(path).update(data)
                // return snap
            ];
        });
    });
}
//--------------------------------- Chat Functions --------------------------------------------------------
//--------------------------------- Chat Functions --------------------------------------------------------
//--------------------------------- Chat Functions --------------------------------------------------------
exports.sendChatToFriend = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, chatMessage, currentStatus, friendChatID, latestMessagePath, nextNumber, nextMessagePath, messagePath, friendUserName, message;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = context.auth.uid;
                fuid = data.friendID;
                chatMessage = data.message;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                currentStatus = _a.sent();
                if (!(currentStatus === sFriend)) return [3 /*break*/, 7];
                friendChatID = createFriendChatID(myUid, fuid);
                latestMessagePath = baseDBPath + '/FriendChats/' + friendChatID + '/latestMessageNumber';
                return [4 /*yield*/, incrementNumberAtPath(latestMessagePath)
                    // console.log('next number', nextNumber, ' type ', typeof nextNumber, ' obj type: ', typeof chatMessage.number)
                ];
            case 2:
                nextNumber = _a.sent();
                // console.log('next number', nextNumber, ' type ', typeof nextNumber, ' obj type: ', typeof chatMessage.number)
                chatMessage.number = nextNumber;
                // console.log('path ', nextMessagePath)
                chatMessage.timeStamp = admin.database.ServerValue.TIMESTAMP;
                chatMessage.numberOfViews = 0;
                console.log('writing chatMessage ', chatMessage);
                nextMessagePath = baseDBPath + '/FriendChats/' + friendChatID + '/chatMeta/latestMessage';
                messagePath = baseDBPath + '/FriendChats/' + friendChatID + '/messages/' + chatMessage.number;
                return [4 /*yield*/, updateValueToDB(chatMessage, messagePath)];
            case 3:
                _a.sent();
                return [4 /*yield*/, updateValueToDB(chatMessage, nextMessagePath)
                    //send the FCM to the friend
                ];
            case 4:
                _a.sent();
                return [4 /*yield*/, getUserName(myUid, baseDBPath)
                    //send FCM
                ];
            case 5:
                friendUserName = _a.sent();
                message = {
                    token: "",
                    notification: {
                        title: friendUserName,
                        body: chatMessage['msgText']
                    },
                    "apns": {
                        "headers": {
                            "apns-priority": "10"
                        },
                        "payload": {
                            "aps": {
                                "sound": "default",
                                "badge": 1
                            }
                        }
                    },
                    data: {
                        action: NEW_FRIEND_CHAT_MESSAGE,
                        chatID: friendChatID
                    }
                };
                return [4 /*yield*/, sendFCMNotification(fuid, message, baseDBPath, sFriendChatNotify)
                    //return
                ];
            case 6:
                _a.sent();
                return [3 /*break*/, 8];
            case 7:
                console.log("You are not friends with that person");
                throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
            case 8: return [2 /*return*/, 'success'];
        }
    });
}); });
exports.joinFriendChat = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, chatMember, currentStatus, chatID, myChatMemberPath;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = context.auth.uid;
                fuid = data.friendID;
                chatMember = data.chatMember;
                console.log('chatMember', chatMember);
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                currentStatus = _a.sent();
                if (!(currentStatus === sFriend)) return [3 /*break*/, 3];
                chatID = createFriendChatID(myUid, fuid);
                myChatMemberPath = baseDBPath + '/FriendChats/' + chatID + '/chatMeta/chatRoomMembers/' + myUid;
                console.log('path', myChatMemberPath);
                return [4 /*yield*/, updateValueToDB(chatMember, myChatMemberPath)];
            case 2:
                _a.sent();
                return [3 /*break*/, 4];
            case 3:
                console.log("You are not friends with that person");
                throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
            case 4: return [2 /*return*/, 'success'];
        }
    });
}); });
exports.clearFriendChatOfAllMessages = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var baseDBPath, myUid, fuid, currentStatus, friendChatID, chatMeta, d, chatPath, metaPath;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //data sent from App:
                /*
                    data["friendID"] = fuid
                    data["chatID"] = chatID    */
                //check preconditions: Authorization
                checkAuth(context);
                baseDBPath = data.dbRoot;
                myUid = context.auth.uid;
                fuid = data.friendID;
                return [4 /*yield*/, getCuurentStatus(myUid, fuid, baseDBPath)];
            case 1:
                currentStatus = _a.sent();
                if (currentStatus !== sFriend) {
                    console.log("You are not friends with that person");
                    throw new functions.https.HttpsError('failed-precondition', 'You are not friends with that person');
                    // return 'failed'
                }
                friendChatID = createFriendChatID(myUid, fuid);
                if (friendChatID === "") {
                    //this could delete EVERYTHING ... don't do it
                    console.log("friendChatID cannot be blank");
                    throw new functions.https.HttpsError('failed-precondition', 'blank chat id');
                    // return 'failed'
                }
                return [4 /*yield*/, getFriendChatMeta(friendChatID, baseDBPath)];
            case 2:
                chatMeta = _a.sent();
                console.log('original meta', chatMeta);
                d = new Date();
                chatMeta.chatRevision = Math.round(d.getTime() / 1000);
                chatMeta.latestMessage = null;
                chatPath = baseDBPath + '/FriendChats/' + friendChatID;
                console.log('deleting friend chat', chatPath);
                return [4 /*yield*/, admin.database().ref(chatPath).remove()
                    //save updated meta
                ];
            case 3:
                _a.sent();
                //save updated meta
                console.log('saving new meta', chatMeta);
                metaPath = baseDBPath + '/FriendChats/' + friendChatID + '/chatMeta';
                return [4 /*yield*/, updateValueToDB(chatMeta, metaPath)];
            case 4:
                _a.sent();
                //return success
                return [2 /*return*/, 'success'];
        }
    });
}); });
function incrementNumberAtPath(path) {
    return __awaiter(this, void 0, void 0, function () {
        var newNumber, ref;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    ref = admin.database().ref(path);
                    return [4 /*yield*/, ref.transaction(function (currentNumber) {
                            // If users/ada/rank has never been set, currentRank will be `null`.
                            if (currentNumber === null) {
                                console.log("increment number is null");
                                newNumber = 1;
                                return newNumber;
                            }
                            console.log("incrementing old number: ");
                            newNumber = parseInt(currentNumber) + 1;
                            console.log("increment old number: ", currentNumber, ' new:', newNumber);
                            return newNumber;
                        })];
                case 1:
                    _a.sent();
                    console.log('returning ', newNumber);
                    return [2 /*return*/, newNumber];
            }
        });
    });
}
function createFriendChatID(myUid, fuid) {
    //create the id
    var chatID;
    if (myUid < fuid) {
        chatID = myUid + '_' + fuid;
    }
    else {
        chatID = fuid + '_' + myUid;
    }
    console.log('chatID: ' + chatID);
    return chatID;
}
function createFriendChatMeta(myUid, fuid, friendChatID, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var metaPath, newMeta;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    metaPath = baseDBPath + '/FriendChats/' + friendChatID + '/chatMeta';
                    newMeta = new Object();
                    newMeta['chatID'] = friendChatID;
                    newMeta['chatTitle'] = 'friend Chat';
                    // newMeta['latestMessageNumber'] = 0
                    console.log('writing ', newMeta + ' to path: ' + metaPath);
                    return [4 /*yield*/, updateValueToDB(newMeta, metaPath)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, newMeta];
            }
        });
    });
}
function getFriendChatMeta(chatID, baseDBPath) {
    return __awaiter(this, void 0, void 0, function () {
        var metaPath, snap, toReturn;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    metaPath = baseDBPath + '/FriendChats/' + chatID + '/chatMeta';
                    console.log("Path: " + metaPath);
                    return [4 /*yield*/, loadValueFromDB(metaPath)];
                case 1:
                    snap = _a.sent();
                    if (snap.exists()) {
                        toReturn = snap.val();
                    }
                    else {
                        // console.log("profile snapshot does not exist")
                        toReturn = "";
                    }
                    // console.log("profile: ", toReturn)
                    return [2 /*return*/, toReturn];
            }
        });
    });
}
exports.joinGroupChat = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var myUid, chatPath, chatMember, chatID, chatTitle, chatMeta, membersPath;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                myUid = context.auth.uid;
                chatPath = data.path;
                chatMember = data.chatMember;
                chatID = data.chatID;
                chatTitle = data.chatTitle;
                console.log('chatMember', chatMember);
                return [4 /*yield*/, getGroupChatMeta(chatPath)];
            case 1:
                chatMeta = _a.sent();
                console.log('chatMeta ', chatMeta);
                if (!(chatMeta === "")) return [3 /*break*/, 3];
                console.log('creating group chat meta', chatPath);
                return [4 /*yield*/, createGroupChatMeta(chatID, chatPath, chatTitle, myUid, chatMember)];
            case 2:
                _a.sent();
                return [3 /*break*/, 5];
            case 3:
                membersPath = chatPath + '/chatMeta/chatRoomMembers/' + myUid;
                console.log('joining group chat meta', membersPath);
                return [4 /*yield*/, updateValueToDB(chatMember, membersPath)];
            case 4:
                _a.sent();
                _a.label = 5;
            case 5: 
            //write a chatMessage for joining
            // const chatMessage = {
            //     fromUid: myUid,
            //     msgText: chatMember.displayName +  ' joined the chat',
            // }
            // await writeGroupChatMessage(chatPath, chatMessage);
            return [2 /*return*/, 'success'];
        }
    });
}); });
function getGroupChatMeta(path) {
    return __awaiter(this, void 0, void 0, function () {
        var metaPath, snap, toReturn;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    metaPath = path + '/chatMeta';
                    console.log("Path: " + metaPath);
                    return [4 /*yield*/, loadValueFromDB(metaPath)];
                case 1:
                    snap = _a.sent();
                    if (snap.exists()) {
                        toReturn = snap.val();
                    }
                    else {
                        // console.log("profile snapshot does not exist")
                        toReturn = "";
                    }
                    // console.log("profile: ", toReturn)
                    return [2 /*return*/, toReturn];
            }
        });
    });
}
function createGroupChatMeta(chatID, path, chatTitle, uid, chatMember) {
    return __awaiter(this, void 0, void 0, function () {
        var metaPath, chatMemberPath, newMeta, d;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    metaPath = path + '/chatMeta';
                    chatMemberPath = '/chatRoomMembers/' + uid;
                    newMeta = new Object();
                    newMeta['chatID'] = chatID;
                    newMeta['chatTitle'] = chatTitle;
                    d = new Date();
                    newMeta['chatRevision'] = Math.round(d.getTime() / 1000);
                    // newMeta['latestMessageNumber'] = 0
                    newMeta[chatMemberPath] = chatMember;
                    console.log('creating META ', newMeta + ' to path: ' + metaPath);
                    return [4 /*yield*/, updateValueToDB(newMeta, metaPath)];
                case 1:
                    _a.sent();
                    return [2 /*return*/, newMeta];
            }
        });
    });
}
exports.leaveGroupChat = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var myUid, chatPath, membersPath, snapshot;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                myUid = context.auth.uid;
                chatPath = data.path;
                membersPath = chatPath + '/chatMeta/chatRoomMembers/' + myUid;
                return [4 /*yield*/, loadValueFromDB(membersPath)];
            case 1:
                snapshot = _a.sent();
                console.log('deleting member from group chat meta', membersPath);
                return [4 /*yield*/, admin.database().ref(membersPath).remove()
                    //     if(snapshot.exists())
                    //     {
                    //         const chatMember = snapshot.val()
                    //         //write a chatMessage for leaving
                    //         const chatMessage = {
                    //             fromUid: myUid,
                    //             msgText: chatMember.displayName +  ' left the chat',
                    //     }
                    //     await writeGroupChatMessage(chatPath, chatMessage);
                    // }
                ];
            case 2:
                _a.sent();
                //     if(snapshot.exists())
                //     {
                //         const chatMember = snapshot.val()
                //         //write a chatMessage for leaving
                //         const chatMessage = {
                //             fromUid: myUid,
                //             msgText: chatMember.displayName +  ' left the chat',
                //     }
                //     await writeGroupChatMessage(chatPath, chatMessage);
                // }
                return [2 /*return*/, 'success'];
        }
    });
}); });
exports.sendChatToGroup = functions.https.onCall(function (data, context) { return __awaiter(void 0, void 0, void 0, function () {
    var myUid, chatPath, chatMessage, chatID, chatType, chatTitle, chatMeta, chatRoomMembers, senderUserName, notifyType, body, message, baseDBPath, _a, _b, _i, i, element, notify, uid;
    return __generator(this, function (_c) {
        switch (_c.label) {
            case 0:
                //check preconditions: Authorization
                checkAuth(context);
                myUid = context.auth.uid;
                chatPath = data.path;
                chatMessage = data.message;
                chatID = data.chatID;
                chatType = data.chatType;
                chatTitle = data.chatTitle;
                /*
                private String fromUid;
                private String msgText;
                private long timeStamp;
                private int number;
                */
                return [4 /*yield*/, writeGroupChatMessage(chatPath, chatMessage)];
            case 1:
                /*
                private String fromUid;
                private String msgText;
                private long timeStamp;
                private int number;
                */
                _c.sent();
                return [4 /*yield*/, getGroupChatMeta(chatPath)];
            case 2:
                chatMeta = _c.sent();
                chatRoomMembers = chatMeta.chatRoomMembers;
                senderUserName = chatRoomMembers[myUid].displayName;
                switch (chatType) {
                    case sFlightType:
                        notifyType = sFlightChatNotify;
                        break;
                    case sLayoverType:
                    default:
                        notifyType = sLayoverChatNotify;
                        break;
                }
                console.log('chatNotifyType ', notifyType);
                body = senderUserName + ': ' + chatMessage['msgText'];
                message = {
                    token: "",
                    notification: {
                        title: chatTitle,
                        body: body
                    },
                    "apns": {
                        "headers": {
                            "apns-priority": "10"
                        },
                        "payload": {
                            "aps": {
                                "sound": "default",
                                "badge": 1
                            }
                        }
                    },
                    data: {
                        action: NEW_GROUP_CHAT_MESSAGE,
                        chatID: chatID
                    }
                };
                baseDBPath = data.dbRoot;
                console.log('chatmembers ', chatRoomMembers);
                console.log('chatmembers type', typeof chatRoomMembers);
                _a = [];
                for (_b in chatRoomMembers)
                    _a.push(_b);
                _i = 0;
                _c.label = 3;
            case 3:
                if (!(_i < _a.length)) return [3 /*break*/, 6];
                i = _a[_i];
                element = chatRoomMembers[i];
                console.log('element ', element);
                notify = element.sendNotifications;
                if (!notify || element.uid === myUid) {
                    console.log('skipping element ', element);
                    return [3 /*break*/, 5];
                }
                uid = element.uid;
                //send FCM
                return [4 /*yield*/, sendFCMNotification(uid, message, baseDBPath, notifyType)];
            case 4:
                //send FCM
                _c.sent();
                _c.label = 5;
            case 5:
                _i++;
                return [3 /*break*/, 3];
            case 6:
                ;
                return [2 /*return*/, 'success'];
        }
    });
}); });
function writeGroupChatMessage(chatPath, chatMessage) {
    return __awaiter(this, void 0, void 0, function () {
        var latestMessagePath, nextNumber, nextMessagePath, messagePath;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    latestMessagePath = chatPath + '/latestMessageNumber';
                    return [4 /*yield*/, incrementNumberAtPath(latestMessagePath)];
                case 1:
                    nextNumber = _a.sent();
                    // console.log('next number', nextNumber, ' type ', typeof nextNumber, ' obj type: ', typeof chatMessage.number)
                    chatMessage.number = nextNumber;
                    // console.log('path ', nextMessagePath)
                    chatMessage.timeStamp = admin.database.ServerValue.TIMESTAMP;
                    chatMessage.numberOfViews = 0;
                    console.log('writing groupChatMessage ', chatMessage);
                    nextMessagePath = chatPath + '/chatMeta/latestMessage';
                    messagePath = chatPath + '/messages/' + chatMessage.number;
                    return [4 /*yield*/, updateValueToDB(chatMessage, messagePath)];
                case 2:
                    _a.sent();
                    return [4 /*yield*/, updateValueToDB(chatMessage, nextMessagePath)];
                case 3:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
